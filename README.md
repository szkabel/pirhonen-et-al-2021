## General information ##

In this repository we provide a collection of scripts that were used for conducting data analysis in the publication: Juho Pirhonen and Abel Szkalisity et al. 2022, Cancer Research (https://doi.org/10.1158/0008-5472.CAN-22-0396)

## Downloading the repository ##

The repository is structured into submodules, hence we recommend using git to acquire the code:

Use the git command

>     git clone  --recurse-submodules https://bitbucket.org/szkabel/pirhonen-et-al-2021.git 

to check out all the related repositories instantly.

## Workflows ##

The repository is structured into 3 independent workflows to perform the following tasks:

* Proteomic data analysis
* TMA image registration and quantification
* Full-slide IHC image quantification

Further information on how to run these workflows can be found below in this readme.

## Environment ##
<a name="markdown-header-environment"></a>
Our data analysis was conducted with R v4.0.3 and Matlab R2018b on a regular 64bit laptop with Windows10 operating system. The proteomic workflow was also tested on Mac, 
and the TMA image registration on all images was run on clusters for faster processing.

To install R and its most frequently used code editor RStudio follow the general guidelines of these software found at:	
	
* R: https://www.r-project.org/ 
* RStudio: https://www.rstudio.com/products/rstudio/download/#download

Running the codes written in Matlab (the TMA registration pipeline and the full-slide IHC quantification) require a license from Mathworks, and available in most academic institutes.
Standalone executables requiring no Matlab-licesnse are available upon request.
	
## Dependencies ##
	
### R packages ###
We are utilizing several R libraries, each of which can be acquired from the following places:

- CRAN, using the standard `install.packages()` function of R *OR*
- Bioconductor using the `BiocManager::install()` function. This requires first installing BiocManager with `install.packages("BiocManager")`.

The list of R packages:
`beepr, biomaRT, cellranger, ComplexHeatmap, data.table, DT, dtplyr, factoextra, furrr, GFA, GGally, ggExtra, ggpmisc, ggraph, gridExtra, htmltools, janitor, lemon, lubridate, magrittr, multidplyr, parallel, pbmcapply, plotly, readxl, scales, survival, survminer, tictoc, tidygraph, tidyverse, VennDiagram`

## Proteomic data analysis ##
<a name="markdown-header-proteomic-data-analysis"></a>

The proteomic data analysis workflow includes the normalization of protein values (batch-effect correction by ComBat, housekeeping normalization), 
pathway expression calculations, principal component analysis (PCA), t-tests between tissue types, volcano plots and Group Factor Analysis.
	
### Source data ###

The proteomics workflow uses and creates several files, most of which should be placed in a so-called "working directory". 
Before running this workflow create your wokring directory and a subfolder named `Data` in it. Download and place the following files into your `Data` folder:

1. P1-5_Pancreas_cancer_Proteins_list.txt containing the proteomic data of patients 1-5, available from PRIDE (https://www.ebi.ac.uk/pride/archive/projects/PXD026371).
2. P6-14_Pancreas_cancer_Proteins_list.txt containing the proteomic data of patients 6-14, available from PRIDE (https://www.ebi.ac.uk/pride/archive/projects/PXD026371).
3. can-22-0396_supplemental_file_1_suppsd1.xlsx (Supplementary File 1) containing the proteomic data of healthy controls, available in our [paper](https://aacrjournals.org/cancerres/article/82/21/3932/709964/Lipid-Metabolic-Reprogramming-Extends-beyond).
4. can-22-0396_supplemental_file_2_suppsd2.xlsx (Supplementary File 2) containing the survival data of the patients in this study, available in our [paper](https://aacrjournals.org/cancerres/article/82/21/3932/709964/Lipid-Metabolic-Reprogramming-Extends-beyond).
5. Create a directory named `reactome` and download into it the following files from https://reactome.org/download/current/ :
	- ReactomePathways.txt
	- ReactomePathwaysRelation.txt
	- UniProt2Reactome_All_Levels.txt
6. Create a directory named `jci-tessa` and download the following files to it from https://insight.jci.org/articles/view/138290#sd :
	- jci.insight.138290.sdt1.xlsx (Supplemental Table 1)
	- jci.insight.138290.sdt2.xlsx (Supplemental Table 2)
	- Download the Supplemental data pdf file to your computer (first pdf), and convert the Supplemental table 2 into an xlsx file. This can be done for instance by opening the pdf in Word, 
	  copy-pasting the table to excel and saving the result file. Name the result file as jci-patients.xlsx .
7. Create a directory named ˙HPM˙ (Human Proteome Map) and download the following files to it from https://www.humanproteomemap.org/download_hpm_data.php . Note that the files are available for download
   after submitting basic information to the form.
	- HPM_gene_level_epxression_matrix_Kim_et_al_052914.xlsx (Gene-level expression matrix)
	- HPM_protein_level_expression_matrix_Kim_et_al_052914.xlsx (Protein-level expression matrix)
	- HPM_protein_level_expression_matrix_Kim_et_al_052914.xlsx (Peptide-level expression matrix)
8. Create a directory named `DeepProteomeAtlas` and download the following file into it from https://www.embopress.org/doi/full/10.15252/msb.20188503 :
	- Table_EV1.xlsx (Table EV1 link in the Supporting Information section, remember to extract the zip file)
9. Create a directory named `cao` and download the following files to it from https://www.sciencedirect.com/science/article/pii/S0092867421009971?via%3Dihub#app2 :
	- 1-s2.0-S0092867421009971-mmc1.xlsx (Table S1)
	- 1-s2.0-S0092867421009971-mmc7.xlsx (Table S7)
10. Create a directory within the `cao` directory titled `linkedOmics` and download the following files to it from: http://www.linkedomics.org/data_download/CPTAC-PDAC/ :
	- proteomics_gene_level_MD_abundance_tumor.cct.txt (Proteome (Gene level, median-normalized intensity, Tumor), row 4)
	- proteomics_gene_level_MD_abundance_normal.cct.txt (Proteome (Gene level, median-normalized intensity, Normal), row 5)
	
	Note that the downloaded files should have .txt extension.

### Configuration ###

Beyond creating a working directory, an output (or result) directory is also needed (this may be empty). The specification of these locations happen in the `Proteomics/R/init/init_vars.R` file.
In the beginning of this file, set the variable `wd` to the working directory and `rootResultPath` to the output directory (specify the paths between quotes so that R interprets these values as strings). 
Note that even on Windows machines, the path should be specified with regular slashes (/), e.g. C:/Users/... and not C:\Users\...

On Unix based systems, one can fasten the computations by using the configuration below in the `Proteomics/R/init/init_vars.R` file. These settings will cause some parts of the code to run in parallel.

```
# Examplary setup for Unix machines
wd = "/path_to_my_working_directory",
mcCoresLow = 1,
mcCoresMid1 = 4,
mcCoresMid2 = 6,
mcCoresHigh1 = 7,
mcCoresHigh2 = 8,
foreachCores = 4,
rootResultPath = "/path_to_my_result_directory"
```

### Running the workflow ###

The entry point for the proteomic data analysis is the `Proteomics/R/pipe.R` file. Run this script to reproduce our findings by sourcing it in R (this can be done e.g. by typing in
`source('path_to_this_repository/Proteomics/R/pipe.R', chdir = T)` to the R console window. Note: remember to specify the location on your own machine.

## TMA image registration ##
<a name="markdown-header-tma-image-registration"></a>
The TMA image registration workflow is used to quantify tissue microarray (TMA) microscopy images that were used to quantify protein compartmentalization with antibodies. 
The different antibody stainings were applied on parallel tissue sections, hence the workflow starts with registering them into a common space. The registered images
were color-deconvolved to separate the hematoxylin and antibody signals, and subjected to Mander's colocalization analysis. 
Additionally, the sample and background areas were identified with Ilastik and used in several stages of the workflow. Note that running this pipeline requires license to the Matlab software.

### Source data ###

We provide a reduced demo dataset at: https://drive.google.com/drive/folders/1Br1uosL5I1_2_caheh7BdQhOSjLr3ba7?usp=sharing for the TMA registration workflow to demonstrate the required input formats and facilitate any possible re-use of this workflow. The full dataset is available upon request.
However, we note that parts of this workflow should be customized for new cases, such as the foreground-background detection with Ilastik, and the color-deconvolutions.

1. The source microscope images should be organized into separate folders according to the stains, as in the `sourceImages` directory of the demo data.
2. Ilastik models should be trained for each stain separately. Training images can be created randomly by running the `Registrations/Matlab/Registration/Ilastik/prepareForIlastik.m` script. 
   Note that the workspace has to be configured in advance, with the `Registrations/Matlab/Registration/setRegistrationPipeParameters.m` script. Once the training images are ready, use Ilastik to
   create trained models for separating the foreground and the background. The trained models should be placed under `wrkDir\ilastik\trainImgs` and named according to the stain nomenclature (see the demo example)
3. The color deconvolution requires representative colors of each stain (both for the antibody and the hematoxylin channels). 
   These can be specified in a separate `singleStainSamples.mat` file located in the work directory, or if the file is not present the workflow prompts the user to select representative colors on the images.
   The prepared file contains a matrix variable named `myStain` (each row is a single channel, 2 rows per antibody, and columns correspond to RGB) specifying these representative colors. One can also use the `qcColDec` GUI to see the effects of deconvolution. For details on the usage of the `qcColDec` GUI please see the whole-slide IHC section below.
 
### Dependencies ###
<a name="markdown-header-dependencies_1"></a>
1. This workflow requires a working Matlab installation. Matlab is a commercial software and requires a license that is available at most academic institutions. Matlab can be obtained from: https://mathworks.com/products/matlab.html
2. The workflow internally calls python, hence a python installation is required (we used python v 3.6.6 in this project), see https://www.python.org/downloads/ for details. 
   Note, that additional (standard) python modules should be installed: numpy, scipy, math, sys, time and os.
3. This workflow requires Ilastik to be installed, available from: https://www.ilastik.org/ .

### Configuration and run ###

The workflow is configured using the `Registrations/Matlab/Registration/setRegistrationPipeParameters.m` script, check the variable descriptions in the comments of the file. 
Note, that the working directory of the demo dataset (and referred to above) is the `targetDir` variable.

Finally, add all code to the Matlab search path by `addpath(genpath("path_to_this_repository"))`, and then run the workflow with the `fullPipe` command, until the `collocalizeSet` script.

In addition to the `setRegistrationParameters` file, one should also specify the manual threshold values for the deconvolved images. The workflow performs the color deconvolution in the `colorDeconvolveSet` script, 
called in the end of the `fullPipe` script. The deconvolved images will be placed in the working directory under the `Deconvolved` and `DeconvolvedSmall` subdirectories. The threshold values can be determined 
by opening the images in an external program for instance in Fiji. Once the satisfactory threshold value is found, it should be specified in the `Registrations/Matlab/Registration/collocalizeSet.m`.

After the threshold values are customized to the image set, finish the run of the `fullPipe.m` script with calling the `collocalizeSet` script.

### Plot creation ###

The main script of the workflow does not prepare the box-plots of the study, it only creates the raw data needed for creating those. There are both Matlab and R (ggplot) figure creation codes available. 

1. First, configure and run the `Registrations/Matlab/Registration/plotBoxPlot.m` script (configuration means that, set the `dataPath` variable to the `CollocResults` subdirectory of your working directory, and 
   specifying the `stainNames` variable to match with your current stains). Note, that there is a possibility to manually exclude certain spots from the analysis by the `ignoreList` variable, if the registration does not satisfy your expectations.
2. Run the `runFullTransformOnIlastikPrediction` script from the Matlab terminal.
3. Run the `createGlobalSampleCoverage` script from the Matlab terminal.
4. The figures of the publication were created with R and ggplot, which can be done with running the `Registrations/R/boxPlot.R` script. The configuration of this script involves setting the `basePath` variable 
   to the same path as the `dataPath` in point 1., and specifying the location for the `TMA_diagnosticInfo.csv` file. This latter file can be used to assign patient IDs and diagnoses to be used for the
   final plots. See the example in the demo dataset.


## Full-slide IHC quantification ##
<a name="markdown-header-full-slide-ihc-quantification"></a>
To further verify the proteomic results of our manuscript we conducted immunohistochemistry (IHC) on whole tissue sections originating from the same patient sample blocks that were used for the proteomics analysis. This workflow requires manual steps performed in different software, hence it is not a standalone script. Nevertheless, here we provide descriptions of the required steps and the custom code we used to ease the quantification of these images. In a nutshell the workflow creates ROIs on the whole-slide images, exports the selected regions as regular images for further analysis, performs color deconvolution and quantifies the results. Some parts of this workflow require Matlab license.

### Source data ###

Due to the size of full-slide screens, examplary images were not uploaded to any repositories but the data is available upon request. The images we used were in .mrxs format as provided by the 3DHISTECH Pannoramic 250 Flash III imager. The workflow works with any file formats that QuPath can handle. Some scripts in this workflow assume that the image file names follow the nomenclature: slideID_antibodyID.ext where slideID and antibodyID may not contain underscores. The ROI export scripts adds extra fields to this nomenclature and the quantification script extracts fields according to this convention.
 
### Dependencies ###

1. ROI generation and export was conducted with QuPath, a free and open-source software for histological image analysis. QuPath can be obtained from: https://qupath.github.io/
2. Color deconvolution and image quantification requires a running Matlab installation (see installation tips above)
3. The plots were created with R, see installation tips above in the Environment section.

### ROI creation and export ###
Open QuPath and create a new project. Import all slides that you wish to analyze. Define ROI classes of interest (Annotation tab on the top-left). In our work we used Tumor, Adjacent and Control classes. Manually go through your slides and create ROIs belonging to each class. It is recommended to verify the annotations with a qualified pathologist. To export the image data underlying the ROIs (images are created from the bounding box of each ROI) and to create binary masks that specify within these bounding box images the actual ROI pixels, run the `Registrations/QuPath/exportFromQuPath_locations.groovy` script. The script can be run in QuPath as follows. Select the `Automate -> Show script editor` item in the menu. In the opening window select `File -> Open...` and locate the script. Remember to specify a valid destination path for the exported images on line 30 of the script. We refer later to that directory as the `export` directory. Finally, you can run the script by clicking `Run -> Run` in the menu or pressing `CTRL + R`.

Additionally, we provide a QuPath script that eases the creation of publication figures by exporting representative ROIs with their outlines burnt to the image in a thickened way (so that the outline is better visible even if the image is about a larger area but downscaled to fit into a figure). For this purpose, define annotation classes named e.g. ROI_h-bl and create rectangular annotations in this class. These can be exported with the `Registrations/QuPath/exportFromQuPath_ROI.groovy` script. Please check the file's documentation (top rows in the script) for the required parameter modifications and implemented ROI classes.

The location export script iterates over the full project (all images) on its own, however the ROI exporter only runs on a single (currently selected) image. To run the ROI outline exporter script for all images in the project use the `Run for project` item in the Script Editor's `Run` menu.

### Color deconvolution ###
<a name="markdown-header-color-deconvolution"></a>
We provide a small GUI in Matlab to perform color deconvolution on a set of images. In our case these were the exported ROI bounding box images, however the GUI should work with any type of image that can be read by 
the Image Processing Toolbox's `imread` function. The GUI is contained in the `Registrations/Matlab/ColorDeconv/qcColDec.fig` file. To work properly the GUI needs functions from the `ThirdParty/ColorDeconvolutionMatlab` directory, so we suggest to run `addpath(genpath("path_to_this_repository"))` to have all necessary functions on Matlab's search path.

The GUI is started by typing `qcColDeconv('directory_of_images_to_be_deconvolved')` in the Matlab command line (replace the directory in the function call, if reproducing our pipeline then this is the `export` directory). Once the GUI is running, a list of images is provided on the right. There are 2 edit fields `Ch1 color` and `Ch2 color` above the image list, they contain the "representative color" for the color deconvolution as RGB triplets. The default values provide a brown (75 49 20) and a blue (50 20 200) color that we used for our IHC experiment. In the top, one can switch between the original and the color deconvolved images for inspecting the result. Once a satisfactory deconvolution is achieved, export the images to a folder by pressing the `Strech & save` button. When asked, specify a directory to which we'll refer as the `deconvolution` directory later.

Technical note: stretching means a min-max normalization across the whole directory (not per image). This is required to avoid negative image values that could arise from the color deconvolution.

The color deconvolution function originates from: https://github.com/jnkather/ColorDeconvolutionMatlab.

### Quantification ###
To measure the total intensity of a color-deconvolved stain over the specified ROIs we provide the `Registrations/Matlab/ColorDeconv/exportROIandColDecToCsv.m` script. Specify the parameters (`export` directory's masks subfolder, the `deconvolution` directory, target csv file, and the channel of interest) in the beginning of the script and run the script. Further information on these parameters are found in the script ifself. This will produce the csv file required for plot generation.

### Plot creation ###
Plots were created in R with the `Registrations/R` script. This script needs 2 csv files: the output file created by the quantification Matlab script (specified in the `tgtFile` variable on line 11 of the `exportROIandColDecToCsv` script) and the `coordinates.csv` file created by the QuPath ROI export (`exportFromQuPath_locations.groovy`) script in the `export` directory. Specify desired output locations for the figures in line 38 and 92.
   
## Contacts ##

In case of any questions regarding this repository feel free to contact Abel Szkalisity at abel.szkalisity@helsinki.fi or Juho Pirhonen at juho.pirhonen@helsinki.fi